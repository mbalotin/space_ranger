#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <GL/glut.h>

#define SENS_ROT	10.0
#define SENS_OBS	1.0
#define SENS_TRANSL	1.0
#define PI 3.1415

#define TAM 1000
#define D 100

int x_ini,y_ini,bot;
GLfloat rotX, rotY, rotX_ini, rotY_ini;
GLfloat obsX, obsY=200, obsZ=400, obsX_ini, obsY_ini, obsZ_ini;
GLfloat fAspect = 1, angle = 45;

GLfloat luzAmbiente[4]={0.2,0.2,0.2,1.0};
GLfloat luzDifusa[4]={0.7,0.7,0.7,1.0};		 // "cor"
GLfloat luzEspecular[4]={1.0, 1.0, 1.0, 1.0};// "brilho"
GLfloat posicaoLuz[4]={0.0, 30.0, 120.0, 1.0};

struct Position{
	float x;
	float y;
	float z;
};

struct Camera{
	Position eye;
	Position center;
	Position up;
	double rotate;
} camera;


struct Controller{
	bool up;
	bool down;
	bool left;
	bool right;
	bool shot;
	bool cam_up;
	bool cam_down;
} controller;

Position teapot;

double navigate = 0;


void PosicionaObservador(void)
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(camera.eye.x*cos(camera.rotate) + teapot.x, camera.eye.y, camera.eye.z*sin(camera.rotate) + teapot.z, teapot.x, teapot.y, teapot.z, camera.up.x, camera.up.y, camera.up.z);

}

// Fun��o usada para especificar o volume de visualiza��o
void EspecificaParametrosVisualizacao(void)
{
	// Especifica sistema de coordenadas de proje��o
	glMatrixMode(GL_PROJECTION);
	// Inicializa sistema de coordenadas de proje��o
	glLoadIdentity();

	// Especifica a proje��o perspectiva
	gluPerspective(angle,fAspect,0.4,5000);

	// Especifica posi��o do observador e do alvo
	PosicionaObservador();
}

void DesenhaChao()
{
	//Flags para determinar a cord de cada quadrado
	bool flagx, flagz;
	//Define a normal apontando para cima
	glNormal3f(0,1,0);

	glBegin(GL_QUADS);
	flagx = false;
	//X varia de -TAM a TAM, de D em D
	for(float x=-TAM; x<TAM; x+=D)
	{
		//Flagx determina a cor inicial
		if(flagx) flagz = false;
		else flagz = true;
		//Z varia de -TAM a TAM, de D em D
		for (float z=-TAM;z<TAM;z+=D)
		{
			//Escolhe cor
			if(flagz)
				glColor3f(0.4,0.4,0.4);
			else
				glColor3f(1,1,1);
			//E desenha o quadrado
			glVertex3f(x,-60,z);
			glVertex3f(x+D,-60,z);
			glVertex3f(x+D,-60,z+D);
			glVertex3f(x,-60,z+D);
			//Alterna cor
			flagz = !flagz;
		}
		//A cada coluna, alterna cor inicial
		flagx = !flagx;
	}
	glEnd();
}

void DefineIluminacao()
{
	// Capacidade de brilho do material
	GLfloat especularidade[4]={1.0,1.0,1.0,1.0};
	GLint especMaterial = 60;

	// Habilita o modelo de coloriza��o de Gouraud
	glShadeModel(GL_SMOOTH);

	// Define a reflet�ncia do material
	glMaterialfv(GL_FRONT,GL_SPECULAR, especularidade);
	// Define a concentra��o do brilho
	glMateriali(GL_FRONT,GL_SHININESS,especMaterial);

	// Ativa o uso da luz ambiente
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, luzAmbiente);

	// Define os par�metros da luz de n�mero 0
	glLightfv(GL_LIGHT0, GL_AMBIENT, luzAmbiente);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, luzDifusa );
	glLightfv(GL_LIGHT0, GL_SPECULAR, luzEspecular );
	glLightfv(GL_LIGHT0, GL_POSITION, posicaoLuz );

	//Desabilita iluminacao para desenhar a esfera
	glDisable(GL_LIGHTING);
	//Desenha esfera na posi��o da fonte de luz
	glPushMatrix();
	glTranslatef(posicaoLuz[0], posicaoLuz[1], posicaoLuz[2]);
	glColor3f(1.0f, 1.0f, 0.0f);
	glutSolidSphere(5, 50, 50);
	glPopMatrix();
	glEnable(GL_LIGHTING);
}

// Fun��o callback chamada para fazer o desenho*sin(camera.rotate)
void Desenha(void)
{
	// Limpa a janela e o depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	EspecificaParametrosVisualizacao();
	DesenhaChao();

	DefineIluminacao();

	glColor3f(0.0f, 0.0f, 1.0f);

	// Desenha o teapot com a cor corrente (solid)
	
    glTranslated( teapot.x, teapot.y, teapot.z );
	glutSolidCone(50,50,5,5);

	glLineWidth(2.5); 
		glColor3f(0.0, 0.5, 0.5);
		glBegin(GL_LINES);
		glVertex3f(camera.eye.x*cos(camera.rotate), 1, camera.eye.z*sin(camera.rotate));
		glVertex3f(0,0,0);
	glEnd();
	

	glutSwapBuffers();
}

// Inicializa par�metros de rendering
void Inicializa (void)
{
	// Especifica que a cor de fundo da janela ser� preta
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	DefineIluminacao();

	// Habilita a defini��o da cor do material a partir da cor corrente
	glEnable(GL_COLOR_MATERIAL);
	//Habilita o uso de ilumina��o
	glEnable(GL_LIGHTING);
	// Habilita a luz de n�mero 0
	glEnable(GL_LIGHT0);
	// Habilita o depth-buffering
	glEnable(GL_DEPTH_TEST);
}

// Fun��o callback chamada quando o tamanho da janela � alterado
void AlteraTamanhoJanela(GLsizei w, GLsizei h)
{
	// Para previnir uma divis�o por zero
	if ( h == 0 ) h = 1;

	// Especifica o tamanho da viewport
	glViewport(0, 0, w, h);

	// Calcula a corre��o de aspecto
	fAspect = (GLfloat)w/(GLfloat)h;

	EspecificaParametrosVisualizacao();
}

// Fun��o callback chamada para gerenciar eventos do mouse
void GerenciaMouse(int button, int state, int x, int y)
{
	if(state==GLUT_DOWN)
	{
		x_ini = x;
		y_ini = y;
		obsX_ini = obsX;
		obsY_ini = obsY;
		obsZ_ini = obsZ;
		rotX_ini = rotX;
		rotY_ini = rotY;
		bot = button;
	}
	else bot = -1;
}

void GerenciaMovim(int x, int y)
{
	if(bot==GLUT_LEFT_BUTTON)
	{
		int deltax = x_ini - x;
		int deltay = y_ini - y;

		rotY = rotY_ini - deltax/SENS_ROT;
		rotX = rotX_ini - deltay/SENS_ROT;
	}

	else if(bot==GLUT_RIGHT_BUTTON)
	{
		int deltaz = y_ini - y;

		obsZ = obsZ_ini + deltaz/SENS_OBS;
	}

	else if(bot==GLUT_MIDDLE_BUTTON)
	{
		int deltax = x_ini - x;
		int deltay = y_ini - y;

		obsX = obsX_ini + deltax/SENS_TRANSL;
		obsY = obsY_ini - deltay/SENS_TRANSL;
	}
	PosicionaObservador();
	glutPostRedisplay();
}

void GerenciaTeclado(unsigned char key,int,int)
{
	if (key == 27) exit(0);

	glutPostRedisplay();
}


void onKeyUp(int key, int x, int y){
	
	if(key == GLUT_KEY_LEFT) controller.left = false;
	if(key == GLUT_KEY_RIGHT) controller.right = false;
	if(key == GLUT_KEY_UP) controller.up = false;
	if(key == GLUT_KEY_DOWN) controller.down = false;
	
	if(key == GLUT_KEY_PAGE_UP) controller.cam_up = false;
	if(key == GLUT_KEY_PAGE_DOWN) controller.cam_down = false;
}

void onKeyDown(int key, int x, int y){
	
	if(key == GLUT_KEY_LEFT) controller.left = true;
	if(key == GLUT_KEY_RIGHT) controller.right = true;
	if(key == GLUT_KEY_UP) controller.up = true;
	if(key == GLUT_KEY_DOWN) controller.down = true;
	
	if(key == GLUT_KEY_PAGE_UP) controller.cam_up = true;
	if(key == GLUT_KEY_PAGE_DOWN) controller.cam_down = true;

}


void timer(int value){

	if(controller.left){
		if (camera.rotate > 2 * PI)
			camera.rotate = 0;
		camera.rotate += 0.05;
	}
	if(controller.right){
		if (camera.rotate < 0.0)
			camera.rotate = 2 * PI;
		camera.rotate -= 0.05;
	}

	if(controller.up || controller.down){
		// calcula coeficiente angula entre a bola e a c�mera (Yb - Ya) / (Xb - Xa) 
		//double a = (camera.eye.z*sin(camera.rotate) - teapot.z) / (camera.eye.x*cos(camera.rotate) - teapot.x) ; 
		double a = camera.eye.z*sin(camera.rotate) / camera.eye.x*cos(camera.rotate) ; 
		float angulo = atan(a);
		printf(" a = %f, arcotangente = %f\n", a, angulo);

		if(controller.up){
			if (teapot.x < TAM) teapot.x += cos(angulo) * 5;
			if (teapot.z < TAM) teapot.z += sin(angulo) * 5;
		}
		if (controller.down){
			if (-1*TAM < teapot.x) teapot.x += cos(angulo  + PI) * 5;
			if (-1*TAM < teapot.z) teapot.z += sin(angulo+ PI) * 5;
		}
		teapot.z = a*teapot.x;
	}
	if (controller.cam_up){
		if(camera.eye.y < 500)
			camera.eye.y += 10.0;
	}
	if (controller.cam_down){
		if(camera.eye.y > 10)
			camera.eye.y -= 10.0;
	}

	glutPostRedisplay();
	glutTimerFunc(33, timer, 0);
}


// Programa Principal
int main(void)
{
    int argc = 0;
	char *argv[] = { (char *)"gl", 0 };

	glutInit(&argc,argv);

	camera.eye.x = 400.0;
	camera.eye.y = 400.0;
	camera.eye.z = 400.0;
	camera.center.x = 0.0;
	camera.center.y = 0.0;
	camera.center.z = 0.0;
	camera.up.x = 0.0;
	camera.up.y = 1.0;
	camera.up.z = 0.0;
	camera.rotate= 45.0;

	teapot.x = 0;
	teapot.y = 0;
	teapot.z = 0;

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(800,600);
	glutCreateWindow("Exercicio de Iluminacao");
	glutDisplayFunc(Desenha);
	glutReshapeFunc(AlteraTamanhoJanela);
	glutMotionFunc(GerenciaMovim);
	glutMouseFunc(GerenciaMouse);
	glutKeyboardFunc(GerenciaTeclado);

	glutTimerFunc(33, timer, 0);
	
	glutSpecialFunc(onKeyDown);
	glutSpecialUpFunc(onKeyUp);
	
	Inicializa();
	glutMainLoop();

	return 0;
}
